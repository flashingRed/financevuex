import Vue from "vue";
import Vuex from "vuex";
import api from "../api/routers.js";
import router from "../router/index.js";

Vue.use(Vuex);

const state = () => ({
    data: [],
    user: [],
    infos: {
        name: '',
        secondName: '',
        email: '',
    },
    routes: {
        register: {
            api: "auth/signup"
        },
        login: {
            api: "auth/login"
        },
        createWallet: {
            api: "wallet/new"
        },
        createTrans: {
            api: "transactions/new"
        }
    },
    userConcat: {
        wallets: []
    }
});

const getters = {
    infos: state => state.infos,

};
const mutations = {
    REGULATE_INFO_MUT(state) {
        // console.log(name, surname, email)
        // console.log('shit');

        // return JSON.parse(localStorage.user.secondName) 
        console.log(JSON.parse(localStorage.user.secondName));

    }

};

const actions = {
    FETCH_USERS({
        commit
    }) {
        api.get({
                api: state().routes.getALL.api
            })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    // commit("POPULATE_USERS", res.data)
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    CHECK_ME({
        commit
    }) {
        if (!localStorage.token) {
            // Редирект по имени компонента
            router.push({
                name: "Enter"
            })
        }
    },
    REGISTER({
        commit
    }) {
        event.preventDefault()

        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        api.post({
                api: state().routes.register.api,
                obj
            })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token

                    router.push({
                        name: "Home"
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    GO_SIGN({
        commit
    }) {
        localStorage.clear()
        router.push({
            name: "Sign"
        })
    },
    SIGN_IN({
        commit
    }) {
        event.preventDefault()

        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        api.post({
                api: state().routes.login.api,
                obj
            })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token
                    router.push({
                        name: "Home"
                    })
                }
            })
            .catch(err => {
                alert(`${err}`)
                console.log(err);
            })
    },
    SAVE_NAME({
        state
    }) {
        let user = JSON.parse(localStorage.user)
        state.infos.name = user.name
    },
    SAVE_EMAIL({
        state
    }) {
        let user = JSON.parse(localStorage.user)
        state.infos.email = user.email
    },
    SAVE_SECONDNAME({
        state
    }) {
        let user = JSON.parse(localStorage.user)
        state.infos.secondName = user.secondName
    },

    REGULATE_INFO({
        commit
    }) {
        let user = JSON.parse(localStorage.user)
        REGULATE_INFO_MUT(user.name, user.secondName, user.email)

    },
    AddWallet({
        state,  
        commit
    }) {



        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })
        obj["user"] = JSON.parse(localStorage.user).id
        console.log(obj);
        state.userConcat.wallets.push(obj)
        console.log(state.userConcat.wallets);
        // api.post({
        //         api: state().routes.createWallet.api,
        //         obj
        //     })
        //     .then(res => {
        //         if (res.status == 200 || res.status == 201 || res.status == 202) {
        //             state.userConcat.wallets.push(obj)
       
        //             state.userConcat = Object.assign(state.userConcat, JSON.parse(localStorage.user))
        //             localStorage.user = JSON.stringify(state.userConcat) 
        //         }
        //     })
        //     .catch(err => {
        //         alert(`${err}`)
        //         console.log(err);
        //     })
        

    },
    AddTrans({
        commit
    }) {

    },

};

export default {
    state,
    getters,
    mutations,
    actions,
};