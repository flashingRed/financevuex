import axios from "axios"

export default () => {
    const options = {}

    options.baseURL = "https://finance-app-wepro.herokuapp.com/"
    options.contentType = "application/json"

    const instanse = axios.create(options)

    return instanse

}